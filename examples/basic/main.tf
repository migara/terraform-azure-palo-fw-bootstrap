provider "azurerm" {
  version = "=2.0.0"
  features {}
}

resource "azurerm_resource_group" "test-rg" {
  name     = var.rg_name
  location = var.location
}

resource "azurerm_storage_account" "test-sa" {
  name                     = var.storage_account_name 
  resource_group_name      = azurerm_resource_group.test-rg.name
  location                 = azurerm_resource_group.test-rg.location
  account_tier             = "Standard"
  account_replication_type = "LRS"
}

module "bootstrap_storage" {
  source = "../../"
  storage_account_name = azurerm_storage_account.test-sa.name
  storage_account_key  = azurerm_storage_account.test-sa.primary_access_key
  change_content = 2 
  change_software = 2
  change_license = 2
  name = "test"
}    
